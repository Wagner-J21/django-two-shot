from django.contrib import admin
from .models import Account, ExpenseCategory, Receipt


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategorysAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "id",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
    )
