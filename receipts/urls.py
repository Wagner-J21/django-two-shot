from django.urls import path
from .views import (
    reciept_list_view,
    create_receipt,
    expense_category_list_view,
    account_list_view,
    create_expense_category,
    create_account,
)

urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path(
        "categories/create/", create_expense_category, name="create_category"
    ),
    path("accounts/", account_list_view, name="account_list"),
    path("categories/", expense_category_list_view, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", reciept_list_view, name="home"),
]
